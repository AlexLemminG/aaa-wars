﻿using UnityEngine;
using System.Collections;

public class BatteryBar : MonoBehaviour {
	CCC player;
	UnityEngine.UI.Slider slider;
	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController> ().GetComponent<CCC> ();
		slider = GetComponent<UnityEngine.UI.Slider> ();
	}
	
	// Update is called once per frame
	void Update () {
		slider.value = player.hp / player.hpInit;
	}
}
