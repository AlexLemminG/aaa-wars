﻿using UnityEngine;
using System.Collections;

public class BeemoDevice : MonoBehaviour {
	CCC ccc;
	NavMeshAgent agent;
	Animator animator;
	CameraScript cameraS;
	// Use this for initialization
	void Start () {
		ccc = GetComponent<CCC> ();
		animator = GetComponentInChildren<Animator> ();
		agent = GetComponent<NavMeshAgent> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		var lookDir = GetComponent<Device> ().lookDir;
		if (animator.GetCurrentAnimatorStateInfo(0).IsName("Fire") && lookDir != Vector3.zero) {
			ccc.transform.rotation = Quaternion.LookRotation (lookDir, Vector3.up);
			ccc.Move (Vector3.zero);
		}

		if (animator.GetFloat ("Speed") < 0.1f) {
			agent.updateRotation = false;
			ccc.transform.rotation = Quaternion.LookRotation (lookDir, Vector3.up);
		} else {
			agent.updateRotation = true;
		}
	}
}
