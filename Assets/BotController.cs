﻿using UnityEngine;
using System.Collections;

public class BotController : MonoBehaviour {
	public CCC enemy;
	CCC ccc;
	NavMeshAgent agent;

	public CCC deviceToGo;


	public void Update(){
		//agent.CalculatePath (player.transform.position, agent.path);
		//agent.Warp(transform.position);
		if (ccc.enabled && enemy) {
			ccc.LookAt (enemy.transform.position);
			//ccc.MoveTo (enemy.transform.position);
		}
	}


	void Follow(){
		if (deviceToGo && ccc.enabled) {
			if (ccc.device || !TeamsManager.inst.EmptyDevices().Contains(deviceToGo)) {
				deviceToGo = null;
			}
			if (deviceToGo) {
				ccc.MoveTo (deviceToGo.transform.position);
				if ((deviceToGo.transform.position - transform.position).sqrMagnitude < 25) {
					ccc.InOutDevice ();
				}
			}
		}
		else
		if (ccc.enabled && enemy) {
			ccc.MoveTo (enemy.transform.position);
		}
		Invoke ("Follow", Random.Range (0.1f, 0.15f));
	}




	void Start(){
		agent = GetComponent < NavMeshAgent> ();
		ccc = GetComponent<CCC> ();
		agent.speed = ccc.maxSpeed;
		agent.updatePosition = true;
		agent.updateRotation = true;
		//enemy = FindObjectOfType<PlayerController> ().GetComponent<CCC>();
		//InvokeRepeating ("WarpAgent", 0, 0.5f);
		Invoke ("FireRandom", Random.Range (1f, 3f));
		Invoke ("Follow", Random.Range (0.2f, 0.3f));
		UpdateEnemy ();
	}


	void UpdateEnemy(){
		enemy = ClosestEnemy ();

		if (!ccc.device && Random.Range (0, 1f) > 0.9f) {
			deviceToGo = ClosestEmptyDevice ();
		}

		Invoke ("UpdateEnemy", Random.Range (0.5f, 5f));
	}

	void FireRandom(){
		if (ccc.enabled && enemy) {
			CCC c = ccc.device ? ccc.deviceCCC : ccc;
			float hitDist = c.canHit ? (c.damageSphere.radius + 2) : 0f;
			if ((ccc.transform.position - enemy.transform.position).magnitude > hitDist) {
				ccc.Fire ();
			} else
				ccc.GiveHit ();
		}
		Invoke ("FireRandom", Random.Range (1f, 2f));
	}

	void WarpAgent(){
		if(Vector3.Distance(transform.position, agent.nextPosition) > 0.2f)
			agent.Warp (transform.position);
	}

	CCC ClosestEnemy(){
		float distToPlayer = DistToPlayer ();
		var enemies = TeamsManager.inst.Enemies (ccc);
		float minDist = float.PositiveInfinity;
		CCC closest = null;
		foreach (CCC en in enemies) {
			float distSq = (en.transform.position - transform.position).sqrMagnitude;
			if (distSq < minDist) {
				closest = en;
				minDist = distSq;
			}
		}

		return Mathf.Abs(distToPlayer- minDist) < 10f ? TeamsManager.inst.player : closest;
	}

	public float DistToPlayer(){
		var player = TeamsManager.inst.player;
		if (TeamsManager.inst.player.team != ccc.team) {
			return (transform.position - player.transform.position).magnitude;
		} else {
			return 1e30f;
		}
	}
	CCC ClosestEmptyDevice(){
		var devices = TeamsManager.inst.EmptyDevices ();
		float minDist = float.PositiveInfinity;
		CCC closest = null;
		foreach (CCC en in devices) {
			float distSq = (en.transform.position - transform.position).sqrMagnitude;
			if (distSq < minDist) {
				closest = en;
				minDist = distSq;
			}
		}

		return closest;
	}
}
