﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public float speed;
	public float damage = 1f;
	public float lifeTime = 3f;
	public CCC owner;
	Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		rb.velocity = transform.forward * speed;
		Invoke ("DestroyThis", lifeTime);
	}
	public int enemyTeam = -1;
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision col){
		col.gameObject.SendMessage ("GetDamage", new Damage(damage, enemyTeam, owner), SendMessageOptions.DontRequireReceiver);
		Invoke ("DestroyThis", 0f);
	}

	void DestroyThis(){
		Destroy (gameObject);
	}
}

public struct Damage{
	public float damage;
	public int teamToDeal;
	public CCC owner;
	public Damage(float damage, int team, CCC owner){
		this.damage = damage;
		this.owner = owner;
		this.teamToDeal = team;
	}
}
