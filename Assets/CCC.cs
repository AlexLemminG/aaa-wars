﻿using UnityEngine;
using System.Collections.Generic;

public class CCC : MonoBehaviour {
	public float maxSpeed;
	public float jumpHeight;
	public SphereCollider useCollider;
	public CCCMesh mesh;
	Animator animator;

	public float rayDamagePerSec;
	public float kickDamage;
	public float fireDamage;

	public float fireCD = 5f;


	public GameObject rayLight;

	void Awake(){
		animator = GetComponentInChildren<Animator> ();
		getInsideSphere = GetComponentInChildren<UseSphere> ();
		damageSphere = GetComponentInChildren<DamageSphere> ();
		agent = GetComponent<NavMeshAgent> ();
		mesh = GetComponentInChildren<CCCMesh> ();
		isDevice = GetComponent<Device> ();
		hpInit = hp;

		if (isDevice)
			Invoke ("Disable", 0.1f);

	}

	void Disable(){
		enabled = false;
	}
	// Use this for initialization
	void Start () {



		agent.speed = maxSpeed;
		agent.acceleration = maxSpeed * 10f;
		agent.updatePosition = true;
		agent.updateRotation = true;

		if (!isDevice)
			TeamsManager.inst.Add (this);
		else
			TeamsManager.inst.emptyDevices.Add (this);

	}
	bool isDevice;
	NavMeshAgent agent;

	public Vector3 wishVelocity;
	public Vector3 velocity;
	// Update is called once per frame
	void FixedUpdate () {
		if (!device) {
			velocity = agent.velocity;
			if (animator)
				animator.SetFloat ("Speed", velocity.magnitude / maxSpeed);
		}
		if (rayActive) {
			rayDamageSphere.DoDamage (Time.fixedDeltaTime * rayDamagePerSec);
		}


	}

	public void Move(Vector3 direction){
		if (device)
			deviceCCC.Move (direction);
		else
			agent.velocity = Vector3.ClampMagnitude(direction, 1f) * maxSpeed;
		//MoveTo (transform.position + direction);
	}

	public void MoveTo(Vector3 position){
		//wishVelocity = Vector3.ClampMagnitude(position - transform.position, 1f) * maxSpeed;
		if (device)
			deviceCCC.MoveTo (position);
		else {
			agent.SetDestination (position);

		}
		
	}
	void SelfDestroy(){
		if (GetComponent<PlayerController> ()) {
			//Destroy (this);
		} else {
			Destroy (gameObject);
		}
	}
	

	public void LookAt(Vector3 position){
		if (device) {
			deviceCCC.LookAt (position);
			return;
		}

		var dir = (position - transform.position);
		LookAtDirection (dir);
	}

	public void LookAtDirection(Vector3 dir){
		if (device) {
			deviceCCC.LookAtDirection (dir);
			return;
		}
		dir.y = 0;
		//transform.rotation = Quaternion.LookRotation (dir, Vector3.up);
		Debug.DrawRay (transform.position + dir, Vector3.up * 1000f, Color.black);
		if (mesh) {
			mesh.LookAtDirection (dir);
		}
		if (isDevice) {
			GetComponent<Device> ().LookAtDirection (dir);
		}
	}

	UseSphere getInsideSphere;
	public DamageSphere damageSphere;

	public void GiveHit(){
		if (device) {
			deviceCCC.GiveHit ();
			return;
		}
		if (canHit)
			animator.SetBool ("GiveHit", true);
		else if (canFire)
			Fire ();
	}

	void LateUpdate(){

		animator.SetBool ("GiveHit", false);
		animator.SetBool ("Fire", false);
	}

	public void DoDamage(){
		damageSphere.DoDamage (kickDamage);
	}

	public int _points = 0;

	public void AddPoints(int points){
		if (isDevice) {
			var user = GetComponent<Device> ().user;
			if (user) {
				user.AddPoints (points);
			}
		} else {
			_points += points;
		}
	}

	public SphereCollider damageCollider;
	[HideInInspector]
	public float hpInit;
	public void GetDamage(Damage damage){
		if (!(damage.teamToDeal == team))
			return;
		if (device)
			return;
		hp -= damage.damage;
		if (hp <= 0 && !dead) {
			if (isDevice) {
				GetComponent<Device> ().DropBatteryAndLowerCharge (damage.owner);
				hp = hpInit;
				if(damage.owner)
					damage.owner.AddPoints(5);
			} else {
				Die ();
				if(damage.owner)
					damage.owner.AddPoints(5);
			}
		}
		//print (damage.damage + " " +gameObject);
	}

	public bool dead;

	void Die(){
		if (dead)
			return;
		dead = true;
		transform.parent = null;
		if(agent)
			Destroy (agent);
		GetComponent<Rigidbody> ().isKinematic = false;
		this.enabled = false;
		if (!isDevice)
			TeamsManager.inst.Remove (this);

		heartLight.enabled = false;
		Invoke ("SelfDestroy", 4f);
	}
	public Light heartLight;
	[Range(0,1)]
	public int team;

	public Device device;
	public CCC deviceCCC;
	public void InOutDevice(){
		if (isDevice) {
			return;
		}
		if (!device) {
			getInsideSphere.Use ();
		}
		else {
			device.UsedBy (null);
			device = null;
			deviceCCC = null;
			GetComponent<Collider> ().isTrigger = false;
			transform.parent = null;
			transform.localScale = Vector3.one;
			transform.localRotation = Quaternion.identity;
			agent.enabled = true;
		}
	}

	public void SetDevice(Device device){
		this.device = device;
		this.deviceCCC = device.GetComponent<CCC> ();
		transform.parent = device.batteryPlace;
		GetComponent<Collider> ().isTrigger = true;
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;
		transform.localScale = device.BatteryScale ();
		agent.enabled = false;
	}

	void OnEnable(){
		//agent.enabled = true;
		agent.speed = maxSpeed;
		animator.enabled = true;
	}

	void OnDisable(){
		//agent.enabled = false;
		agent.speed = 0f;
		animator.enabled = false;
	}

	float nextFireTime;
	public void Fire(){
		if (device) {
			deviceCCC.Fire ();
		} else {
			if (canFire) {
				if (Time.time >= nextFireTime) {
					animator.SetBool ("Fire", true);
					nextFireTime = Time.time + fireCD;
				}
			} else {
				if (canHit) {
					GiveHit ();
				}
			}
		}
	}

	public bool canFire;
	public bool canHit;

	public void BeginRayDamage(){
		rayActive = true;
		rayLight.SetActive (true);
	}

	public void EndRayDamage(){
		rayActive = false;
		rayLight.SetActive (false);

	}
	bool rayActive = false;
	public DamageSphere rayDamageSphere;


	public void SpawnBullet(){
		//print ("pew");
		var go = (GameObject)Instantiate (bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
		go.GetComponent<Bullet> ().damage = fireDamage;
		go.GetComponent<Bullet> ().enemyTeam = team == 0 ? 1 :((team == 1) ? 0 : -1);
		go.GetComponent<Bullet> ().owner = this;
	}

	public GameObject bullet;
	public Transform bulletSpawnPoint;

	public float hp = 10;

	public void LowerCharge(CCC damager){
		GetDamage (new Damage(10, team, damager));
	}
}