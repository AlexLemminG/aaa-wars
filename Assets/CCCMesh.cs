﻿using UnityEngine;
using System.Collections;

public class CCCMesh : MonoBehaviour {
	CCC ccc;
	public Transform arms;
	// Use this for initialization
	void Start () {
		ccc = GetComponentInParent<CCC> ();
	}

	void DoDamage(){
		ccc.DoDamage ();
	}

	void SpawnBullet(){
		ccc.SpawnBullet ();
	}

	void BeginRayDamage(){
		ccc.BeginRayDamage ();
	}

	void EndRayDamage(){
		ccc.EndRayDamage ();
	}

	bool looked = false;
	public void LookAtDirection(Vector3 dir){
		looked = true;
		if (arms) {
			dir.y = 0;
			var up = new Vector3 (-dir.z, dir.y, dir.x);
			arms.rotation = Quaternion.LookRotation (-dir, up);
		}
	}

	void Update(){
		if (!looked) {
			LookAtDirection (ccc.transform.forward);
		}
		looked = false;
	}
}
