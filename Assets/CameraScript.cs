﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
	new Camera camera;

	public CCC toFollow;
	public float distNoDevice = 50f;
	public float distDevice = 150f;
	public Vector3 delta;
	// Use this for initialization
	void Awake () {
		camera = GetComponent<Camera> ();
	}

	void Start(){

		dist = toFollow.device ? distDevice : distNoDevice;
		delta = transform.position - toFollow.transform.position;
		delta = transform.localPosition;
		transform.SetParent (null);
	}

	float dist;
	// Update is called once per frame
	void LateUpdate () {
		var wishDist = toFollow.device ? distDevice : distNoDevice;
		dist = Mathf.Lerp (dist, wishDist, Time.deltaTime);
		var wishPos = WishPos1 ();
		transform.position = Vector3.Lerp (transform.position, wishPos, Time.deltaTime*5f);

		var localPos = toFollow.transform.InverseTransformPoint (transform.position);
		var xz = new Vector2 (localPos.x, localPos.z);
		if (xz.sqrMagnitude < 49) {
			var xznorm = xz.normalized;
			var xzrot = new Vector2 (xznorm.y, -xznorm.x);
			xz = xznorm * 19 + xzrot * 1 * Time.deltaTime;
			xz = xz.normalized * 7;
		}
		localPos = new Vector3 (xz.x, localPos.y, xz.y);
		//transform.position = toFollow.transform.TransformPoint (localPos);

		//transform.LookAt (toFollow.transform.position, Vector2.up);
	}

	Vector3 WishPos2(){
		return toFollow.transform.TransformPoint (delta.normalized * dist);
	}

	Vector3 WishPos1(){
		return toFollow.transform.position + delta.normalized * dist;
	}

	public Vector3 GetMouseProjection(Vector3 defaultReturn = new Vector3()){
		var ray = camera.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit)) {
			Debug.DrawRay (hit.point, Vector3.up * 10);
			return hit.point;
		} else
			return defaultReturn;
	}

	
}
