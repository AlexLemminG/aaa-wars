﻿using UnityEngine;
using System.Collections;

public class DamageSphere : MonoBehaviour {
	public float radius{
		get{
			if (col is SphereCollider) {
				return ((SphereCollider)col).radius;
			}
			return col.bounds.extents.magnitude;
		}
	}

	public float rad;
	public void DoDamage(float damage){
		col.enabled = true;
		this.damage = damage;
		frames = 0;
	}
	float damage;
	int frames;
	void FixedUpdate(){
		frames += 1;
		if(frames > 1)
			col.enabled = false;

		rad = radius;
	}


	CCC ccc;
	Collider col;
	//CCC ccc;
	void Start(){
		col = GetComponent<Collider> ();
		//ccc = GetComponentInParent<CCC> ();
		col.enabled = false;
		ccc = GetComponentInParent<CCC> ();
	}

	void OnTriggerStay(Collider other){
		//print ("DAMAGEE");
		other.gameObject.SendMessage ("GetDamage", new Damage(damage, ccc.team == 0 ? 1 : (ccc.team == 1 ? 0 : -1), ccc), SendMessageOptions.DontRequireReceiver);
	}
}
