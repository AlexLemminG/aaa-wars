﻿using UnityEngine;
using System.Collections;

public class Device : MonoBehaviour {
	public CCC user;
	CCC ccc;
	// Use this for initialization
	void Start () {
		ccc = GetComponent<CCC> ();	
	}
	public void UsedBy(CCC user){
		if (user) {
			user.GetComponent<CCC> ().SetDevice (this);
			ccc.enabled = true;
			TeamsManager.inst.emptyDevices.Remove (ccc);
		} else {
			ccc.enabled = false;
			TeamsManager.inst.emptyDevices.Add (ccc);

		}
		this.user = user;
		ccc.team = user ? user.team : -1;
	}

	public bool IsEmpty(){
		return !user;
	}

	public void DropBatteryAndLowerCharge(CCC damager){
		if (user) {
			user.LowerCharge (damager);
			user.InOutDevice ();
		}
	}

	public void LookAtDirection(Vector3 dir){
		lookDir = dir;
	}

	public Vector3 lookDir;

	public Transform batteryPlace;

	public Vector3 BatteryScale(){
		return transform.lossyScale * scale;
	}
	public float scale = 1f;

	void Update(){
		if (user) {
			if (user.hp <= 0)
				DropBatteryAndLowerCharge (ccc);
		}
	}
}
