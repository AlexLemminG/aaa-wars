﻿using UnityEngine;
using System.Collections;

public class DynoDevice : MonoBehaviour {
	CCC ccc;
	NavMeshAgent agent;
	Animator animator;
	// Use this for initialization
	void Start () {
		ccc = GetComponent<CCC> ();
		animator = GetComponentInChildren<Animator> ();
		agent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (animator.GetCurrentAnimatorStateInfo(0).IsName("GiveHit")) {
			if(agent.desiredVelocity.magnitude < agent.speed / 10f)
				ccc.Move (ccc.transform.forward);

		}
	}
}
