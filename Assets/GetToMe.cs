﻿using UnityEngine;
using System.Collections;

public class GetToMe : MonoBehaviour {
	UI ui;
	// Use this for initialization
	void Start () {
		ui = FindObjectOfType<UI> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider col){
		if (col.CompareTag ("Player") && ui.timeLeft <= 0) {
			ui.Victory ();
		}
	}
}
