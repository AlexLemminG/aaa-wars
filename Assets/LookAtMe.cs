﻿using UnityEngine;
using System.Collections;

public class LookAtMe : MonoBehaviour {
	public Transform[] lookies;
	// Use this for initialization
	void Start () {
	
	}
	public float speed;
	// Update is called once per frame
	void Update () {
		float delta = Time.deltaTime * speed;
		for (int i = 0; i < lookies.Length; i++) {
			lookies [i].Rotate (new Vector3 (0f, 0f, delta * (i+1)));
			delta = delta * -1;
		}
	}
}
