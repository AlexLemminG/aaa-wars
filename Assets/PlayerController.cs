﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	CCC ccc;
	CCC deviceCCC;
	CameraScript cameraS;
	NavMeshAgent agent;
	// Use this for initialization
	void Start () {
		ccc = GetComponent<CCC> ();
		cameraS = FindObjectOfType<CameraScript> ();


		agent = GetComponent < NavMeshAgent> ();
		ccc = GetComponent<CCC> ();
		agent.speed = ccc.maxSpeed;

		agent.updatePosition = true;
		agent.updateRotation = true;

		//InvokeRepeating ("WarpAgent", 0, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		float h = Input.GetAxisRaw ("Horizontal");
		float v = Input.GetAxisRaw ("Vertical");
		bool use = Input.GetKeyDown (KeyCode.F);
		bool giveHit = Input.GetButton ("Fire2");
		bool fire = Input.GetButton ("Fire1");
		var dir = new Vector3 (h, 0, v);
		//dir = cameraS.transform.TransformVector (dir);
		dir.y = 0f;
		dir = dir.normalized;

		deviceCCC = ccc.deviceCCC;

		var forwardPos = deviceCCC ? deviceCCC.transform.position + deviceCCC.transform.forward : ccc.transform.position + ccc.transform.forward;
		var position = deviceCCC ? deviceCCC.transform.position: ccc.transform.position;
		//var forwardPos = tr.position + tr.forward;
		Vector3 lookAt = cameraS.GetMouseProjection(forwardPos);

		if (ccc.dead) {
			FindObjectOfType<UI> ().Loose ();
			enabled = false;
		}

		if (ccc.enabled) {
			ccc.Move(dir);

			Debug.DrawRay (position + dir, Vector3.up * 1000f);
			if (use)
				ccc.InOutDevice ();
			if (giveHit)
				ccc.GiveHit ();

			if (fire) {
				ccc.Fire ();
			}

			if (fire || giveHit || dir.magnitude < 0.05f) {
				ccc.LookAt (lookAt);
			}
		}

	}

	void OnDestroy(){

		//FindObjectOfType<UI> ().Loose ();
	}
}
