﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
	public GameObject prefab;
	public int maxAmount = 20;
	public int team;
	UI ui;
	public float minTime = 1f;
	public float maxTime = 1f;

	void Start(){
		ui = FindObjectOfType<UI> ();
		if(prefab)
			Invoke("Spawn", Random.Range(minTime, maxTime));
	}

	void Spawn(){
		var list = team == 0 ? TeamsManager.inst.team0 : TeamsManager.inst.team1;
		if(list.Count < maxAmount && ui.timeLeft > 0)
			Instantiate (prefab, transform.position, Quaternion.identity);
		Invoke("Spawn", Random.Range(minTime, maxTime));
	}
}
