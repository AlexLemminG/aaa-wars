﻿using UnityEngine;
using System.Collections.Generic;

public class TeamsManager : MonoBehaviour {
	
	public static TeamsManager inst{
		get{
			if (!_inst)
				_inst = FindObjectOfType<TeamsManager> ();
			return _inst;
		}
	}
	private static TeamsManager _inst;
	public CCC player;

	void Start(){
		player = FindObjectOfType<PlayerController> ().GetComponent<CCC> ();
	}

	public List<CCC> team0;
	public List<CCC> team1;
	public List<CCC> emptyDevices;


	public List<CCC> Enemies(CCC you){
		if (you.team == 0)
			return team1;
		else
			return team0;
	}

	public void Add(CCC ccc){
		if (ccc.team == 0)
			team0.Add (ccc);
		else
			team1.Add (ccc);
	}


	public void Remove(CCC ccc){
		if (ccc.team == 0)
			team0.Remove (ccc);
		else
			team1.Remove (ccc);
	}

	public List<CCC> EmptyDevices(){
		return emptyDevices;
	}
}
