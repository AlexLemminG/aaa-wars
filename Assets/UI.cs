﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI : MonoBehaviour {
	public float timeLeft = 120f;
	public Text timer;
	public Text survive;
	public Text score;

	public Text goToGamepad;
	// Use this for initialization
	void Start () {
		
	}

	bool noTime = false;
	// Update is called once per frame
	void Update () {
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0 && !noTime) {
			noTime = true;
			timer.gameObject.SetActive (false);
			survive.gameObject.SetActive (false);
			goToGamepad.gameObject.SetActive (true);
			foreach(var sp in FindObjectsOfType<Spawner>()){
				sp.enabled = false;
			}
		}
		score.text = "Score: " + TeamsManager.inst.player._points;
		timer.text = (((int)timeLeft) / 60) + " : " + ((int)timeLeft % 60);
	}

	public GameObject victory;
	public GameObject loose;
	public void Victory(){
		if(!loose.activeSelf)
			victory.SetActive (true);
	}

	public void Loose(){
		if(!victory.activeSelf)
			loose.SetActive (true);
	}
}
