﻿using UnityEngine;
using System.Collections;

public class Usable : MonoBehaviour {
	public bool usable = true;


	public void Use(CCC user){
		if(usable)
			BroadcastMessage ("UsedBy", user);
	}
}
