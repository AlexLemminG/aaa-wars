﻿using UnityEngine;
using System.Collections;

public class UseSphere : MonoBehaviour {

	public void Use(){
		col.enabled = true;
		frames = 0;
	}
	public int frames;

	void FixedUpdate(){
		
		frames += 1;
		if(frames > 1)
			col.enabled = false;
	}

	

	Collider col;
	CCC ccc;
	void Start(){
		col = GetComponent<Collider> ();
		ccc = GetComponentInParent<CCC> ();
		col.enabled = false;
	}

	void OnTriggerStay(Collider other){
		var device = other.GetComponentInParent<Device> ();
		if (device) {
			if (device.IsEmpty()) {
				device.UsedBy (ccc);
			}
		}
	}
}
