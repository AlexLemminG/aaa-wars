﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class VictoryPanel : MonoBehaviour {
	public Text score;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		score.text = "Your score: " + TeamsManager.inst.player._points;
	}

	public void Replay(){
		
		SceneManager.LoadScene ("Room");
	}
}
